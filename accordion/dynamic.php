<?php 
// The below code uses a repeater field from Advanced Custom Fields, and assigned the accordion button a unique id.
?>


<div class="faq-container accordion">
    <?php if ( have_rows( 'faq' ) ) : ?>
    <?php while ( have_rows( 'faq' ) ) : the_row(); ?>
    <div class="accordion-item">
        <button id="accordion-button-<?php echo get_row_index(); // uses the row index, which creates the unique id ?>" aria-expanded="false"><span class="accordion-title"><?php the_sub_field( 'question' ); ?></span><span class="icon" aria-hidden="true"></span></button>
        <div class="accordion-content">
            <?php the_sub_field( 'answer' ); ?>
        </div>
    </div>
    <?php endwhile; ?>
    <?php else : ?>
    <?php // no rows found ?>
    <?php endif; ?>
</div>